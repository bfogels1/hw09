//Ali Geisa and Brian Fogelson
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class JHUgle {
    public static void main(String[] args) throws IOException {
        // create some file and command line readers.
        FileReader fileInput = new FileReader(args[0]);
        BufferedReader fileReader = new BufferedReader(fileInput);
        InputStreamReader commandLineInput = new InputStreamReader(System.in);
        BufferedReader commandLineReader = new BufferedReader(commandLineInput);
        Map<String, Set<String>> map = new TreapMap<>();
        buildMap(fileReader, map);
        // run the program.
        runProg(commandLineReader, map);
        // close the readers.
        //System.out.print(map); // for testing
        commandLineInput.close();
        commandLineReader.close();
        fileInput.close();
        fileReader.close();
    }

    private static void buildMap(BufferedReader file, Map<String,
                                  Set<String>> map)
        throws IOException {
        String url;
        String line;
        while((url = file.readLine()) != null) {
            //url = file.readLine();
            line = file.readLine();
            String[] keys = line.split("\\s+");//change to split over whitespace
            for (String key: keys) {
                //addToMap(key, url, map);
                if (map.has(key)) {
                    map.get(key).insert(url);
                }
                else {
                    Set<String> set = new ArraySet<>();
                    set.insert(url);
                    map.insert(key, set);
                }
            }
        }
    }

    private static void runProg(BufferedReader commandLine,
                                Map<String, Set<String>> map)
        throws IOException {
        String line;
        Stack<Set<String>> stack = new Stack<>();
        //String ops = "||&&?";
        System.out.print("> ");
        while(!(line = commandLine.readLine()).equals("!")) {
            int op = isOp(line);
            if (op != 0) {
                // index should be the hash map basically
		Set<String> set = runOps(stack, op, map); // maybe index
                if (set.size() != 0) {
                    for (String s: set) {
                        System.out.println(s);
                    }
                }
            } else {
                //addToStack(line, stack);
                if (map.has(line)) {
                    stack.push(map.get(line));
                } else {
                    //KEY NOT FOUND DONT PRINT ANY RESULTS
                    stack.push(new ArraySet<String>());
                }
            }
            System.out.print("> ");
        }
    }

    /*
    private void addToMap(String key, String url, TreapMap<String,
                          String[]> map) {
        //make case to make new array
        String[] prevArray = map.find(key);
        String[] tempArray = [url];
        map.put(key, concat(prevArray,tempArray));
    }
    */
    
    /*
    private static void addToStack(String s, Stack<Set<String>> stack) {
            String[] temp = new String[1];
            temp[0] = s;
            stack.push(temp);
    }
    */

    private static int isOp(String s) {
        if (s.equals("?")) {
            return 1;
        } else if (s.equals("||")) {
            return 2;
        } else if (s.equals("&&")) {
            return 3;
        }
        return 0;
    }

    
    private static Set<String> runOps(Stack<Set<String>> stack, int op,
                               Map<String, Set<String>> map) {
        if (op == 1) {
            //run some shit on stack.top();
            return stack.peek();
            //catch empty Stack Exception
        }
        else {
            return popOps(stack, op, map);
        }
    }


    
    private static Set<String> popOps(Stack<Set<String>> stack, int op,
                               Map<String, Set<String>> map) {
        if (!stack.empty()) {
            Set<String> old1 = stack.pop();
            if (!stack.empty()) {
                Set<String> old2 = stack.pop();
                //String[] newArray = concat(old1, old2);
                Set<String> newSet = new ArraySet<>();
                if (op == 2) { // OR
                    for (String s: old1) {
                        newSet.insert(s);
                    }
                    for (String s: old2) {
                        newSet.insert(s);
                    }
                    stack.push(newSet);
                    return newSet;
                } else { // AND
                    for (String s: old1) {
                        if (old2.has(s)) {
                            newSet.insert(s);
                        }
                    }
                    stack.push(newSet);
                    return newSet;
                }
                //newArray = concat(newArray, line);
            } else { // could only remove one thing so put it back
                stack.push(old1);
                //PRINT SOME ERROR MAYBE
            }
        } else {
             //System.err.print error or something
        }
        return new ArraySet<String>();
    }
    

    /*
    //Probably want to change from string array to other data struct
    //without so much allocation
    private static String[] concat(String[] a, String[] b) {
        String[] newArray = new String[a.length + b.length];
        System.arraycopy(a, 0, newArray, 0, a.length);
        System.arraycopy(b, 0, newArray, 0, b.length);
        return newArray;
    }
    */
}
