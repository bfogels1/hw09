//Brian Fogelson and Ali Geisa
public class SearchEngine extends Map<K, V> {
    void insert(K k, V v) throws IllegalArgumentException {
        //Insert
    }

    V remove(K k) throws IllegalArgumentException {
        //remove
    }

    void put(K k, V v) throws IllegalArgumentException {
        //put
    }

    V get(K k) throws IllegalArgumentException {
        //get
    }

    boolean has(K k) {
        //has
    }

    int size() {
        //size
    }
}
