//Brian Fogelson and Ali Geisa
public class HashMap<K, V> {
    private int size;
    private int used;
    private ArrayWithOwner<V, K>[] array; //might need to fix this for trees.
    //private TreapMap<K, V> 

    public HashMap() {
        array = (ArrayWithOwner<V, K>[]) new Object[];
    }

    //not complete for trees?? - for collisions
    private int find (K k) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].owner.equals(k)) {
                return i;
            }
        }
    }
    
    int hash(K k) {
        //Bernstein hash
        int h;
        String str = k.toString();
        for (int i = 0; i < this.size; i++) {
            h = 33 * h + str.charAt(i);
        }
        return h;
    }

    int compress(int i) {
        //Bitmask And Mod by length
        i &= 0x7fffffff;
        i %= this.size;
    }
        
    void insert(K k, V v) throws IllegalArgumentException {
        //Insert
        //First, Hash then compress key
        int index = compress(hash(k));
        ArrayWithOwner<V, K> newArrayWithOwner = new ArrayWithOwner<>(k);
        array[index] = newArrayWithOwner;
    }

    V remove(K k) throws IllegalArgumentException {
        //remove
    }

    void put(K k, V v) throws IllegalArgumentException {
        //put
    }

    V get(K k) throws IllegalArgumentException {
        //get
    }

    boolean has(K k) {
        //has
    }

    int size() {
        //size
    }
}
