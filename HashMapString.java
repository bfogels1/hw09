//Brian Fogelson and Ali Geisa
//String is key, string[] is url's
public class HashMapString<K, V[]> implements Map {
    private int size;
    private int used;
    //private ArrayWithOwner<V, K>[] array; //might need to fix this for trees.
    private TreapMap<K, V[]>[] array; 

    public HashMap() {
        array = (TreapMap<K, V[]>[]) new Object[];
    }

    //find index of a key
    private int find (K k) {
        int i = compress(hash(k));
        for (K k2: array[i]) {
            if (k2.equals(k)) {
                return i;
            }
        }
    }
    
    int hash(K k) {
        //Bernstein hash
        int h;
        String str = k.toString();
        for (int i = 0; i < str.size; i++) {
            h = 33 * h + str.charAt(i);
        }
        return h;
    }

    int compress(int i) {
        //Bitmask And Mod by length
        i &= 0x7fffffff;
        i %= this.size;
    }

    @Override
    void insert(K k, V v) throws IllegalArgumentException {
        //Insert
        //First, Hash then compress key
        int index = compress(hash(k));
        ArrayWithOwner<V, K> newArrayWithOwner = new ArrayWithOwner<>(k);
        array[index] = newArrayWithOwner;
    }

    @Override
    V remove(K k) throws IllegalArgumentException {
        //remove
    }

    @Override
    void put(K k, V v) throws IllegalArgumentException {
        //put
    }

    @Override
    V get(K k) throws IllegalArgumentException {
        //get
    }

    @Override
    boolean has(K k) {
        //has
    }

    @Override
    int size() {
        //size
    }
}
