public class ArrayWithOwner<T, O> {
    private T[] array;
    private O owner;
    private int used;

    public ArrayWithOwner(O o) {
        this.owner = o;
        this.array = (T[]) new Object[10];
    }


    private void resize() {
        T[] newArray = (T[]) new Object[this.array.length * 2];
        //Array copy
        System.arraycopy(this.array, 0, newArray, this.array.length, 0,
                         this.array.length);
        this.array = newArray;
    }

    
    void add(T t) {
        while(this.used >= this.array.length) {
            this.resize();
        }
        this.array[this.used] = t;
        this.used++;
    }

    T get(int i) {
        return array[i];
    }

    int size() {
        return array.length;
    }
}
